package com.imooc.dao;

import com.imooc.vo.User;

import java.util.List;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-13 13:04
 **/
public interface UserDao {
    User getUserByUserName(String userName);

    List<String> queryRolesByUserName(String userName);

}
