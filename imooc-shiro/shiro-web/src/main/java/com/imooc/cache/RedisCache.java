package com.imooc.cache;

import com.imooc.util.JedisUtil;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.springframework.stereotype.Component;
import org.springframework.util.SerializationUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-18 22:21
 **/
@Component
public class RedisCache<K,V> implements Cache<K,V> {
    @Resource
    private JedisUtil jedisUtil;

    private final String CACHE_PREFIX = "xm-cache";

    private byte[] getKey(K k) {
        if (k instanceof String) {
            return (CACHE_PREFIX + k).getBytes();
        }
        return SerializationUtils.serialize(k);
    }
    @Override
    public V get(K k) throws CacheException {
        System.out.println("从redi获取授权");
        byte[] value = jedisUtil.get(getKey(k));
        if (value != null) {
            return (V) SerializationUtils.deserialize(value);
        }
        return null;
    }

    @Override
    public V put(K k, V v) throws CacheException {
        byte[] key = getKey(k);
        byte[] value = SerializationUtils.serialize(k);
        jedisUtil.set(key, value);
        jedisUtil.expire(key, 600);
        return v;
    }

    @Override
    public V remove(K k) throws CacheException {
        byte[] key = getKey(k);
        byte[] value = jedisUtil.get(key);
        jedisUtil.del(key);
        if (value != null) {
            return (V) SerializationUtils.deserialize(value);
        }
        return null;
    }

    @Override
    public void clear() throws CacheException {
        Set<byte[]> keys = jedisUtil.keys(SerializationUtils.serialize(CACHE_PREFIX).toString());
        if (!keys.isEmpty()) {
            for (byte[] key : keys) {
                jedisUtil.del(key);
            }
        }
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Set<K> keys() {
      return (Set<K>) jedisUtil.keys(SerializationUtils.serialize(CACHE_PREFIX).toString());
    }

    @Override
    public Collection<V> values() {
        Set<byte[]> keys = (Set<byte[]>) jedisUtil.keys(SerializationUtils.serialize(CACHE_PREFIX).toString());
        Set<V> values = new HashSet<V>();
        for (byte[] key : keys) {
            V value = (V) SerializationUtils.deserialize(jedisUtil.get(key));
            values.add(value);
        }

        return values;
    }
}
