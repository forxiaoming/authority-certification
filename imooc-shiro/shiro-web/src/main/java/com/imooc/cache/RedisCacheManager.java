package com.imooc.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

import javax.annotation.Resource;

/**
 * shiro缓存管理
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-18 22:19
 **/
public class RedisCacheManager implements CacheManager {
    @Resource
    private RedisCache redisCache;

    @Override
    public <K, V> Cache<K, V> getCache(String s) throws CacheException {
        return redisCache;
    }
}
