-- shiro 中 jdbcRealm
create table if not exists  users(
  `id` int primary key not null  auto_increment,
  `username` varchar(225),
  `password` varchar(225),
  `password_salt` varchar(225)
);
insert into users(username,password)
values ('xiaoming',"123");
select * from users;

create table if not exists user_roles(
  `id` int primary key not null auto_increment,
  `role_name` varchar(225),
  `username` varchar(225)
);
insert into user_roles(role_name, username)
VALUES('admin','xiaoming');
select * from user_roles;

create table if not exists roles_permissions (
  id int primary key not null auto_increment,
  `permission` varchar(225),
  `role_name` varchar(225)
);
insert into roles_permissions(permission, role_name)
values ('user:select', 'admin');
select * from roles_permissions;
