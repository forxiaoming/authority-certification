import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

/**
 * IniRealm 测试
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-11 17:32
 **/
public class IniRealmTest {

    @Test
    public void testIniRealm() {
        IniRealm iniRealm = new IniRealm("classpath:user.ini");

        // 1. 创建SecurityManager环境
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        defaultSecurityManager.setRealm(iniRealm);

        // 环境设置
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        // 2. 主体认证
        Subject subject = SecurityUtils.getSubject();

        UsernamePasswordToken token = new UsernamePasswordToken("xiaoming", "123");
        subject.login(token);

        // 验证
        if (subject.isAuthenticated()) {
            System.out.println("登录成功");
            // 检查角色
            subject.checkRoles("admin");
            // 检查权限
            subject.checkPermission("user:delete");
            subject.checkPermission("user:update");
        }
    }
}
