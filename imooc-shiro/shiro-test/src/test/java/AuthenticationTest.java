import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Before;
import org.junit.Test;

public class AuthenticationTest {

    SimpleAccountRealm simpleAccountRealm = new SimpleAccountRealm();

    /**
     * 认证之前 在Realm中添加一个用户
     */
    @Before
    public void addUser(){
        simpleAccountRealm.addAccount("xiaoming", "123","admin","user","guest");
    }

    /**
     * Shiro 认证 测试
     */
    @Test
    public void testAuthentication(){

        // 1. 创建SecurityManager环境
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        defaultSecurityManager.setRealm(simpleAccountRealm);
        // 2. 主体认证提交
        // 设置环境
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        // 主体
        Subject subject = SecurityUtils.getSubject();
        // token
        UsernamePasswordToken token = new UsernamePasswordToken("xiaoming", "123");

        // 登录
        subject.login(token);
        //  shiro提供的一个是否认证的方法
        System.out.println(subject.isAuthenticated());
        subject.logout();
        System.out.println(subject.isAuthenticated());

        // 分析login()
        /*
        首先调用 securityManager.login() , 然后调用 authenticate(token);
        然后调用 doAuthenticate(token). 然后调用doSingleRealmAuthentication() 获取了realm完成认证过程
         */

     }

    /**
     * Shiro 授权 测试
     */
    @Test
     public void testAuthorization() {
        // 1. 创建SecurityManager 环境
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        defaultSecurityManager.setRealm(simpleAccountRealm);
        // 2. 主体认证提交
        // 设置环境
        SecurityUtils.setSecurityManager(defaultSecurityManager);

        Subject subject = SecurityUtils.getSubject();
        // token
        UsernamePasswordToken token = new UsernamePasswordToken("xiaoming", "123");
        subject.login(token);

        // 检查角色
        if(subject.isAuthenticated()){
            System.out.println("登录成功");

            // org.apache.shiro.authz.UnauthorizedException: Subject does not have role [admin2]
            try {
                subject.checkRoles("admin", "admin2");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
     }
}
