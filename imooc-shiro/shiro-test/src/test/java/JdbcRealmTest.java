import com.alibaba.druid.pool.DruidDataSource;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Test;


/**
 * JdbcRealm 测试
 * @author xiaoming
 * @version 1.0
 * @date 2019-04-11 21:38
 **/
public class JdbcRealmTest {

    DruidDataSource dataSource = new DruidDataSource();
    {
        dataSource.setUrl("jdbc:mysql://118.24.117.178:3306/testDB");
        dataSource.setUsername("testDB");
        dataSource.setPassword("testDB");
    }

    @Test
    public void testJdbcRealmTest() {
        JdbcRealm jdbcReallm = new JdbcRealm();
        jdbcReallm.setDataSource(dataSource);
        // 需要开启权限查询
        jdbcReallm.setPermissionsLookupEnabled(true);

//        JdbcRealm默认提供了查询SQL, 也可以设置自定义SQL
//        jdbcReallm.setAuthenticationQuery("select password from users where username = ?");
//        jdbcReallm.setUserRolesQuery("select role_name from user_roles where username = ?");
//        jdbcReallm.setPermissionsQuery("select permission from roles_permissions where role_name = ?");

        // 1. 创建环境
        DefaultSecurityManager securityManager = new DefaultSecurityManager();
        // 设置 realm
        securityManager.setRealm(jdbcReallm);

        // 2. 主体提交认证
        // token
        UsernamePasswordToken token = new UsernamePasswordToken("xiaoming", "123");
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();
        subject.login(token);

        // 检查
        if(subject.isAuthenticated()){
            System.out.println("登录成功");
            // 检查角色
            subject.checkRoles("admin");
            // 检查权限(需要jdbcReelm 开启权限查询)
            // Subject does not have permission [user:delete]
            subject.checkPermission("user:delete");

        }

    }

}
