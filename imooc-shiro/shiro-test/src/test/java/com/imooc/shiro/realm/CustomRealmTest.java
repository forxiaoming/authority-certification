package com.imooc.shiro.realm;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomRealmTest {
    CustomRealm customRealm = new CustomRealm();

    /**
     * 测试认证
     */
    @Test
    public void testCustomRealm() {
        // 1. 创建SecurityManager环境
        DefaultSecurityManager securityManager = new DefaultSecurityManager();
        securityManager.setRealm(customRealm);
        //  shiro加密 (shiro会自动对密码加密解密)
        HashedCredentialsMatcher matcher = new HashedCredentialsMatcher();
        matcher.setHashAlgorithmName("md5");
        matcher.setHashIterations(1);
        customRealm.setCredentialsMatcher(matcher);

        // 2. 主体认证提交
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("xiaoming", "123");
        subject.login(token);

        // 检查登录
        if (subject.isAuthenticated()) {
            System.out.println("登录成功");
            // 检查权限
            subject.checkRole("admin");
            subject.checkPermission("user:add");
        }

    }
}